PAra las carnes:

    S�cala de la nevera 2 o 3 horas antes.
    Aseg�rate de que la parrilla est� muy caliente antes de poner la carne.
    No empieces a cocinar hasta que no est�n las brasas blanquecinas y sin llama.
    Si echamos sal gorda en las brasas durar�n m�s tiempo y provocar�n menos humo.
    Las piezas grandes las separamos m�s de las brasas para que se hagan a fuego lento y evitar qu� se quemen por fuera y queden crudas por dentro.
    Usa sal gorda y a��dela en la segunda vuelta o justo antes de retirar la carne de la parrilla.
    Puedes aromatizar las carnes a�adiendo a las brasas: tomillo, romero, salvia o hinojo.
    No pinches los alimentos como el chorizo, la morcilla, las salchichas� si lo haces perder�n su jugo y quedar�n m�s secos.
    Para que las costillas queden muy jugosas y tiernas debes hervirlas durante una hora, secarlas muy bien, untarlas con salsa barbacoa y ponerlas en la parrilla.
    Las brochetas de carne no las untes con aceite o mantequilla, �ntalas con salsa de tomate, mostaza o hierbas arom�ticas, se formar� una sabrosa costra y la carne mantendr� todo su sabor.
